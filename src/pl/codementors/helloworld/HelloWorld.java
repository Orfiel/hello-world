package pl.codementors.helloworld;

import java.util.Scanner;

/**
 * Welcome class.
 * @author psysiu
 */
public class HelloWorld {

    /**
     * Welcome method.
     * @param args Application starting parameters.
     */
    public static void main(String[] args) {
        System.out.println("Hello World!");

        //poniżej są przechowywane wartości
        byte byteVar = 1;
        short shortVar = 1;
        int intVar = 1;
        long longVar = 1;
        float floatVar = 1;
        double doubleVar = 1;
        boolean booleanVar = true;
        //char charVar = 'a'; przesunięty do komentarza by nie ingerował

        /* ukrycie wyświetlania wartości
        System.out.println(byteVar);
        System.out.println(shortVar);
        System.out.println(intVar);
        System.out.println(longVar);
        System.out.println(floatVar);
        System.out.println(doubleVar);
        System.out.println(booleanVar);
        //System.out.println(charVar);
        */
        //proźba o wartość byte
        System.out.println("Podaj wartość dla byte");
        //polecenie odbierające dane od urzytkownika
        Scanner inputScanner = new Scanner(System.in);
        //wczytanie wartości nextByte (poniżej analogicznie)
        byteVar = inputScanner.nextByte();
        System.out.println("Podaj wartość dla short");
        shortVar = inputScanner.nextShort();
        System.out.println("Podaj wartość dla int");
        intVar = inputScanner.nextInt();
        System.out.println("Podaj wartość dla long");
        longVar = inputScanner.nextLong();
        System.out.println("Podaj wartość dla float");
        floatVar = inputScanner.nextFloat();
        System.out.println("Podaj wartość dla double");
        doubleVar = inputScanner.nextDouble();
        System.out.println("Podaj wartość dla boolean !UWAGA! przyjmuje jedynie wartości 'true' i 'false'");
        booleanVar = inputScanner.nextBoolean();


        //Scanner inputScanner = new Scanner(System.in); wystarczy jednorazowe wskazanie input
        //byteVar = inputScanner.nextByte();
        //wyświetla przyjęte wartości
        System.out.println("Byte = "+byteVar);
        System.out.println("Short = "+shortVar);
        System.out.println("Int = "+intVar);
        System.out.println("Long = "+longVar);
        System.out.println("Float = "+floatVar);
        System.out.println("Double = "+doubleVar);
        System.out.println("Boolean = "+booleanVar);

    }
}
